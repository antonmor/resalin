<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('header');
		$this->load->view('singup');
		$this->load->view('footer');
	}
	
	public function vpanel(){
		    $this->load->view('header');
		  	$this->load->view('panel');
		  	$this->load->view('footer');
	}
	public function validar(){
		$this->load->model('Bd');
		$this->load->library('session');
		$usuario=$this->input->post('user');
		$pwd=$this->input->post('password');
		$query=$this->Bd->singup($usuario,$pwd);
		   if (count($query) > 0){
		   	
		   	$arraysession = array(
		   		'idsp' =>$query[0]->idpersona, 
		   		'nombre' =>$query[0]->nombre, 
		   		'idrol' =>$query[0]->idroles, 
		   		'email' =>$query[0]->user,
		   		'idplantel' =>$query[0]->idplantel, 
		   		'singup' =>TRUE 
		   		);
		   	$this->session->set_userdata($arraysession);
		     } else {
		   	 print_r("1");
		   }
		   
	}
	public function singout()
	{
		$user_data = array('singup' => FALSE);
		$this->session->set_userdata($user_data);
		$this->session->sess_destroy();
		redirect('Welcome/index');
	}
	public function asistencia(){
		$this->load->view('head2');
		//$this->load->view('barrasup');
		$this->load->view('barraizq');
		//$this->load->view('welcome_message');
		$this->load->view('footer');

	}

}
