-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`plantel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`plantel` (
  `idplantel` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificador del plantel',
  `plantel` VARCHAR(45) NULL COMMENT 'plantel de la dgeti, eje: cetis 157 ,cbtis 157',
  `nombre` VARCHAR(45) NULL,
  `domicilio` TINYTEXT NULL,
  `falta` TIMESTAMP(20) NULL,
  `activo` INT NULL COMMENT 'Corresponde al plantel activo.',
  PRIMARY KEY (`idplantel`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`persona`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`persona` (
  `idpersona` INT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(45) NULL,
  `app` VARCHAR(45) NULL,
  `apm` VARCHAR(45) NULL,
  `sexo` VARCHAR(45) NULL COMMENT 'Se refiere al genero: Masculino o Femenino',
  `fechnac` DATE NULL COMMENT 'Fecha de nacimiento de la persona',
  `curp` VARCHAR(18) NULL COMMENT 'Se refiere a la Clave  Única de Registro de Población',
  `nocontrol` VARCHAR(45) NULL COMMENT 'Numero de control del estudiante o clave del personal',
  `activo` INT NULL COMMENT 'Corresponde a la persona activo por el plantel, eje: 0 inactivo, 1 activo',
  `plantel_idplantel` INT NOT NULL,
  PRIMARY KEY (`idpersona`),
  UNIQUE INDEX `id_UNIQUE` (`idpersona` ASC),
  INDEX `fk_persona_plantel_idx` (`plantel_idplantel` ASC),
  CONSTRAINT `fk_persona_plantel`
    FOREIGN KEY (`plantel_idplantel`)
    REFERENCES `mydb`.`plantel` (`idplantel`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`rol`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`rol` (
  `idrol` INT NOT NULL AUTO_INCREMENT COMMENT 'Corresponde al identificador del rol del usuario del sistema',
  `rol` VARCHAR(45) NULL COMMENT 'Corresponde al rol del usuario del sistema, alumno, padre de familia, docente, administrativo, enfermero',
  `frol` TIMESTAMP(18) NULL COMMENT 'corresponde a la fecha cuando se crea el rol, asignada por el sistema',
  `activo` INT NULL COMMENT 'Corresponde al rol activo por el plantel, eje: 0 inactivo, 1 activo',
  PRIMARY KEY (`idrol`),
  UNIQUE INDEX `idrol_UNIQUE` (`idrol` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`materia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`materia` (
  `idmateria` INT NOT NULL AUTO_INCREMENT COMMENT 'Corresponde al identificador de la materia',
  `materia` VARCHAR(45) NULL COMMENT 'Corresponde al nombre de la materia eje: álgebra, cálculo, entre otras. ',
  `fcmateria` TIMESTAMP(18) NULL COMMENT 'Corresponde a la fecha en que se crea la materia.',
  `activo` INT NULL COMMENT 'Corresponde a la materia activa por el plantel, eje: 0 inactivo, 1 activo',
  PRIMARY KEY (`idmateria`),
  UNIQUE INDEX `idmateria_UNIQUE` (`idmateria` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`especialidad`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`especialidad` (
  `idespe` INT NOT NULL AUTO_INCREMENT,
  `clvesp` VARCHAR(45) NULL COMMENT 'Corresponde a ',
  `especialidad` VARCHAR(45) NULL,
  `activo` INT NULL COMMENT 'Corresponde a la especialidad activa por el plantel, eje: 0 inactivo, 1 activo',
  `idplantel` INT NOT NULL COMMENT 'Identificador de plantel que contiene la especialidad',
  PRIMARY KEY (`idespe`),
  UNIQUE INDEX `idespe_UNIQUE` (`idespe` ASC),
  INDEX `fk_especialidad_plantel1_idx` (`idplantel` ASC),
  CONSTRAINT `fk_especialidad_plantel1`
    FOREIGN KEY (`idplantel`)
    REFERENCES `mydb`.`plantel` (`idplantel`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`roles` (
  `idroles` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla',
  `idpersona` INT NOT NULL COMMENT 'Corresponde al identificador de la persona',
  `idrol` INT NOT NULL COMMENT 'Corresponde al identificador del rol de la persona',
  `fini` DATE NULL COMMENT 'Corresponde al inicio del periodo del rol a desempeñar ',
  `ffin` DATE NULL COMMENT 'Corresponde al fin del periodo del rol desempeñado',
  `activo` INT NULL COMMENT 'Corresponde a los roles activos.',
  PRIMARY KEY (`idroles`),
  INDEX `fk_persona_has_rol_rol1_idx` (`idrol` ASC),
  INDEX `fk_persona_has_rol_persona1_idx` (`idpersona` ASC),
  UNIQUE INDEX `idroles_UNIQUE` (`idroles` ASC),
  CONSTRAINT `fk_persona_has_rol_persona1`
    FOREIGN KEY (`idpersona`)
    REFERENCES `mydb`.`persona` (`idpersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_has_rol_rol1`
    FOREIGN KEY (`idrol`)
    REFERENCES `mydb`.`rol` (`idrol`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`carga`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`carga` (
  `idcarga` INT NOT NULL AUTO_INCREMENT,
  `idespe` INT NOT NULL,
  `idmateria` INT NOT NULL,
  `fcarga` TIMESTAMP(18) NULL COMMENT 'Corresponde a la fecha en que se crea el programa',
  `clave` VARCHAR(45) NULL COMMENT 'Corresponde a la clave del programa',
  `activo` INT NULL COMMENT 'Corresponde a la carga activo.',
  PRIMARY KEY (`idcarga`),
  INDEX `fk_especialidad_has_materia_materia1_idx` (`idmateria` ASC),
  INDEX `fk_especialidad_has_materia_especialidad1_idx` (`idespe` ASC),
  CONSTRAINT `fk_especialidad_has_materia_especialidad1`
    FOREIGN KEY (`idespe`)
    REFERENCES `mydb`.`especialidad` (`idespe`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_especialidad_has_materia_materia1`
    FOREIGN KEY (`idmateria`)
    REFERENCES `mydb`.`materia` (`idmateria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`grupos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`grupos` (
  `idgrupos` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla grupos',
  `grupo` TINYTEXT NULL COMMENT 'Corresponde al grupo dentro de la especialidad del plantel.',
  `semestre` INT NULL COMMENT 'Corresponde al semestre registrado en el sistema',
  `activo` INT NULL COMMENT 'Corresponde al grupo que se mantiene activo en el sistema 0:inactivo 1: activo',
  PRIMARY KEY (`idgrupos`),
  UNIQUE INDEX `idgrupos_UNIQUE` (`idgrupos` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`asignacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`asignacion` (
  `idasignacion` INT NOT NULL AUTO_INCREMENT,
  `idcarga` INT NOT NULL,
  `idgrupos` INT NOT NULL,
  `fini` DATETIME NULL COMMENT 'Corresponde la fecha inicio del ciclo (semestre)',
  `ffin` DATETIME NULL COMMENT 'Corresponde la fecha fin del ciclo (semestre)',
  `periodo` VARCHAR(45) NULL COMMENT 'Corresponde al periodo del ciclo, ejem: feb-jun2018',
  `activo` INT NULL COMMENT 'Corresponde al ',
  PRIMARY KEY (`idasignacion`),
  INDEX `fk_carga_has_grupos_grupos1_idx` (`idgrupos` ASC),
  INDEX `fk_carga_has_grupos_carga1_idx` (`idcarga` ASC),
  UNIQUE INDEX `idasignacion_UNIQUE` (`idasignacion` ASC),
  CONSTRAINT `fk_carga_has_grupos_carga1`
    FOREIGN KEY (`idcarga`)
    REFERENCES `mydb`.`carga` (`idcarga`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_carga_has_grupos_grupos1`
    FOREIGN KEY (`idgrupos`)
    REFERENCES `mydb`.`grupos` (`idgrupos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`kardex`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`kardex` (
  `identificador` INT NOT NULL AUTO_INCREMENT COMMENT 'identificador de relación donde el estudiante se encuentra en una especialidad, grupo y semestre',
  `persona_idpersona` INT NOT NULL,
  `especialidad_idespe` INT NOT NULL,
  `grupos_idgrupos` INT NOT NULL,
  PRIMARY KEY (`identificador`, `persona_idpersona`, `especialidad_idespe`),
  INDEX `fk_persona_has_especialidad_especialidad1_idx` (`especialidad_idespe` ASC),
  INDEX `fk_persona_has_especialidad_persona1_idx` (`persona_idpersona` ASC),
  INDEX `fk_persona_has_especialidad_grupos1_idx` (`grupos_idgrupos` ASC),
  UNIQUE INDEX `id_UNIQUE` (`identificador` ASC),
  CONSTRAINT `fk_persona_has_especialidad_persona1`
    FOREIGN KEY (`persona_idpersona`)
    REFERENCES `mydb`.`persona` (`idpersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_has_especialidad_especialidad1`
    FOREIGN KEY (`especialidad_idespe`)
    REFERENCES `mydb`.`especialidad` (`idespe`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_persona_has_especialidad_grupos1`
    FOREIGN KEY (`grupos_idgrupos`)
    REFERENCES `mydb`.`grupos` (`idgrupos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`horario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`horario` (
  `idhorario` INT NOT NULL AUTO_INCREMENT,
  `hin` TIME(10) NULL,
  `hfin` TIME(10) NULL,
  `lunes` VARCHAR(45) NULL,
  `martes` VARCHAR(45) NULL,
  `miercoles` VARCHAR(45) NULL,
  `jueves` VARCHAR(45) NULL,
  `viernes` VARCHAR(45) NULL,
  `sabado` VARCHAR(45) NULL,
  `idasignacion` INT NOT NULL,
  `activo` INT NULL COMMENT 'Corresponde si esta activo 0:inactivo 1: activo',
  PRIMARY KEY (`idhorario`),
  UNIQUE INDEX `idhorario_UNIQUE` (`idhorario` ASC),
  INDEX `fk_horario_asignacion1_idx` (`idasignacion` ASC),
  CONSTRAINT `fk_horario_asignacion1`
    FOREIGN KEY (`idasignacion`)
    REFERENCES `mydb`.`asignacion` (`idasignacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`asistencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`asistencia` (
  `idasis` INT NOT NULL AUTO_INCREMENT COMMENT 'Identificador de la tabla',
  `idasig` INT NOT NULL COMMENT 'Corresponde al identificador de la programa que esta vigente en la especialidad ',
  `idmateria` INT NOT NULL COMMENT 'Corresponde al identificador de la materia donde está el docente y el estudiante',
  `idgrupos` INT NOT NULL COMMENT 'Corresponde al identificador del grupo',
  `idest` INT NOT NULL COMMENT 'id de persona alumno',
  `iddoc` INT NOT NULL COMMENT 'Corresponde al identificador de la tabla persona en el cual se toman los roles de tutor o docente asignado a un grupo.',
  `asistencia` TIMESTAMP(12) NULL COMMENT 'fecha de asistencia o falta',
  `asiste` INT NULL COMMENT 'Corresponde a si asiste o no, eje: 0:no asistir, 1:si asiste',
  `obs` TEXT(100) NULL COMMENT 'Corresponde a las observaciones que realice el docente ',
  `activo` INT NULL COMMENT 'Corresponde a si el registro esta activo :0inactivo, 1:activo',
  PRIMARY KEY (`idasis`),
  INDEX `fk_asignacion_has_persona_persona1_idx` (`idest` ASC),
  UNIQUE INDEX `idasis_UNIQUE` (`idasis` ASC),
  INDEX `fk_asistencia_grupos1_idx` (`idgrupos` ASC),
  INDEX `fk_asistencia_materia1_idx` (`idmateria` ASC),
  INDEX `fk_asistencia_asignacion1_idx` (`idasig` ASC),
  INDEX `fk_asistencia_persona1_idx` (`iddoc` ASC),
  CONSTRAINT `fk_asignacion_has_persona_persona1`
    FOREIGN KEY (`idest`)
    REFERENCES `mydb`.`persona` (`idpersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_asistencia_grupos1`
    FOREIGN KEY (`idgrupos`)
    REFERENCES `mydb`.`grupos` (`idgrupos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_asistencia_materia1`
    FOREIGN KEY (`idmateria`)
    REFERENCES `mydb`.`materia` (`idmateria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_asistencia_asignacion1`
    FOREIGN KEY (`idasig`)
    REFERENCES `mydb`.`asignacion` (`idasignacion`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_asistencia_persona1`
    FOREIGN KEY (`iddoc`)
    REFERENCES `mydb`.`persona` (`idpersona`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`ci_sessions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`ci_sessions` (
  `id` VARCHAR(128) NOT NULL,
  `ip_address` VARCHAR(45) NOT NULL,
  `timestamp` INT NOT NULL DEFAULT 0,
  `data` BLOB NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC),
  INDEX `ci_sessions_timestamp` (`timestamp` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
